# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'slack_test_reporter'

Gem::Specification.new do |spec|
  spec.name          = 'slack_test_reporter'
  spec.version       = SlackTestReporter::VERSION
  spec.authors       = ['GitLab Quality']
  spec.email         = ['gitlab-qa@gitlab.com']

  spec.summary       = 'Parse test results from various sources and post reports to Slack'
  spec.homepage      = 'https://gitlab.com/gitlab-org/quality/junit-slack-reporter'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`
                         .split("\x0").reject { |f| f.match(%r{^spec/}) }
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'pry', '~> 0.11'
  spec.add_development_dependency 'rake', '~> 12.2'
  spec.add_development_dependency 'rspec', '~> 3.7'
  spec.add_development_dependency 'rubocop', '~> 0.82.0'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.36'
  spec.add_development_dependency 'factory_bot', '~> 6.1'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.4.2'
  spec.add_runtime_dependency 'nokogiri', '~> 1.10'
  spec.add_runtime_dependency 'memoist', '~> 0.16'
  spec.add_runtime_dependency 'activesupport-inflector', '~> 0.1'
end
