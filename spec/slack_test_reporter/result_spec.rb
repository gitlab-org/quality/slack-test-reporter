# frozen_string_literal: true

module SlackTestReporter
  RSpec.describe Result do
    describe 'attributes' do
      it { should respond_to :status, :name, :classname, :file, :execution_time, :output, :stack_trace }
      it { should respond_to :name=, :classname=, :file=, :execution_time=, :output=, :stack_trace= }
    end

    describe 'statuses' do
      using RSpec::Parameterized::TableSyntax

      where(:given_status, :method, :val) do
        'passed' | :passed?  | true
        :passed  | :passed?  | true
        :passed  | :failed?  | false
        :passed  | :skipped? | false

        'failed' | :failed?  | true
        :failed  | :failed?  | true
        :failed  | :passed?  | false
        :failed  | :skipped? | false

        'skipped' | :skipped? | true
        :skipped  | :skipped? | true
        :skipped  | :passed?  | false
        :skipped  | :failed?  | false
      end

      with_them do
        before { subject.status = given_status }

        it 'returns the appropriate status' do
          expect(subject.public_send(method)).to eq(val)
        end
      end
    end

    describe '#passed?' do
      it 'returns true when test passes' do
        subject.status = :passed
        expect(subject).to be_passed
      end
    end

    describe '#status' do
      it 'returns a symbol' do
        subject.status = 'passed'
        status = subject.status
        expect(status).to be_a Symbol
        expect(status).to eq(:passed)
      end
    end
  end
end
