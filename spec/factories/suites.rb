# frozen_string_literal: true

FactoryBot.define do
  factory :suite do
    name { 'Test Suite' }
    time { 1000.00 }
    total_count { 100 }
    total_successes { 25 }
    total_failures { 25 }
    total_skipped { 25 }
    total_errors { 25 }
    error_message { 'Error' }
  end
end
