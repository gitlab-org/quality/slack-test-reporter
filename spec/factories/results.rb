# frozen_string_literal: true

FactoryBot.define do
  factory :result do
    status { :passed }
    name { 'Test result' }
    classname { 'module.test_result' }
    file { 'spec/module/test_result.rb' }
    execution_time { 0.03 }
    output { nil }
    stack_trace { nil }
  end
end
