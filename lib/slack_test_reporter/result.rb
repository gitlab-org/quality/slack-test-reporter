# frozen_string_literal: true

module SlackTestReporter
  # Representation of a Test Result
  class Result
    attr_reader :status
    attr_accessor :name,
                  :classname,
                  :file,
                  :execution_time,
                  :output,
                  :stack_trace

    # Check if the test had passed
    # @return [Boolean] true if the test had passed
    def passed?; status == :passed; end

    # Check if the test had been skipped
    # @return [Boolean] true if the test had been skipped
    def skipped?; status == :skipped; end

    # Check if the test had failed
    # @return [Boolean] true if the test had failed
    def failed?; status == :failed; end

    # Set the status of the test result
    def status=(status)
      @status = status.to_sym
    end
  end
end
