# frozen_string_literal: true

module SlackTestReporter
  VERSION = '0.0.1'

  autoload :Result, 'slack_test_reporter/result'
  autoload :Suite, 'slack_test_reporter/suite'
  autoload :Report, 'slack_test_reporter/report'
  autoload :Runner, 'slack_test_reporter/runner'

  autoload :ReportParser, 'slack_test_reporter/report_parser'

  module Parser
    autoload :Junit, 'slack_test_reporter/parser/junit'
  end
end
